# this is a comment
# do not delete this part
# to use this project locally, install a live-server:
# python live-server / Atom Editor, atom-live-server / VSCode, Live Server / etc.
#
# overwrite txt/your_text_here.txt with the following syntax:
#
# syntax: s, [p]
# > "s" string: the alphanumeric symbols to be displayed
# > "p" integer: the distribution method of the alphanumeric symbols; word-by-word (0), one-chunk (1), letter-by-letter (2) (optional, default vaule is "0")
# example:
# the 1st segment will be displayed in one chunk
"one morning, when", 1
# the 2nd segment will be displayed word-by-word
"Gregor Samsa"
# the 3rd segment will be displayed in one chunk
"woke from troubled", 1
# the 4th segment will be displayed word-by-word
"dreams"
# the 5th segment will be displayed word-by-word
"he found himself"
# the 6th segment will be displayed letter-by-letter
"transformed", 2
# the 7th segment will be displayed in one chunk
"in his bed", 1
# the 8th segment will be displayed word-by-word
"into a horrible"
# the 9th segment will be displayed letter-by-letter
"vermin", 2
