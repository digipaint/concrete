var cnv = {
  w: undefined,
  h: undefined
}
var font
var txt
var segmentCounter = 0
var placing = {
  state: false,
  pos: {
    x: undefined,
    y: undefined
  },
  offset: {
    x: undefined,
    y: undefined
  },
  distance: undefined,
  size: 0.015,
  rotation: undefined
}
var placed = []
var capture
var poseNet
var pose
var skeleton
var state = {
  menu: true,
  mouse: false,
  webcam: false
}

function preload() {
  font = loadFont('ttf/AnonymousPro-Bold.ttf')
  txt = loadStrings('txt/your_text_here.txt')
}

function setup() {
  cnv.h = windowHeight * 0.95
  cnv.w = windowWidth * 0.95
  txt = textProcessor(txt)
  capture = createCapture(VIDEO)
  capture.hide()
  poseNet = ml5.poseNet(capture, modelLoaded)
  poseNet.on('pose', gotPoses)
}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose
    skeleton = poses[0].skeleton
  }
}

function modelLoaded() {
  console.log('poseNet ready')
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(0)
  rectMode(CENTER)
  textFont(font)
  blendMode(DIFFERENCE)
  colorMode(RGB, 255, 255, 255, 1)

  if (state.menu === true) {
    textSize(cnv.w * 0.2)
    textAlign(CENTER, TOP)
    fill(255)
    noStroke()

    push()
    translate(windowWidth * 0.5, 10)
    text('concrete', cnv.w * 0.0125, 0)
    pop()

    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    if (mouseX <= windowWidth * 0.5) {
      rect(-0.25 * windowWidth, 0, windowWidth * 0.5, windowHeight)
    } else {
      rect(0.25 * windowWidth, 0, windowWidth * 0.5, windowHeight)
    }
    if (mouseX <= windowWidth * 0.5) {
      textSize(cnv.w * 0.1)
    } else {
      textSize(cnv.w * 0.05)
    }
    text('mouse', -cnv.w * 0.25, 0)
    if (mouseX > windowWidth * 0.5) {
      textSize(cnv.w * 0.1)
    } else {
      textSize(cnv.w * 0.05)
    }
    text('webcam', cnv.w * 0.25, 0)
    pop()
  }

  if (state.mouse === true) {
    textAlign(LEFT, TOP)
    textSize(cnv.w * 0.015)
    fill(255)
    noStroke()
    push()
    translate(windowWidth * 0.5 - cnv.w * 0.49, windowHeight * 0.5 - cnv.h * 0.49)
    if (segmentCounter < txt.length) {
      text('current segment:', 0, 0)
    } else {
      text('done!', 0, 0)
    }
    push()
    translate(cnv.w * 0.15, 0)
    text(txt[segmentCounter], 0, 0)
    pop()

    pop()
    push()
    translate(windowWidth * 0.5 + cnv.w * 0.375, windowHeight * 0.5 - cnv.h * 0.49)
    text('undo: backspace', 0, 0)
    pop()

    blendMode(DIFFERENCE)
    textAlign(CENTER, CENTER)
    for (var i = 0; i < placed.length; i++) {
      textSize(cnv.w * placed[i][3])
      push()
      translate(windowWidth * 0.5 + placed[i][1] * cnv.w, windowHeight * 0.5 + placed[i][2] * cnv.h)
      rotate(placed[i][4])
      noStroke()
      fill(255)
      text(placed[i][0], 0, 0)
      pop()
    }

    blendMode(DIFFERENCE)
    textAlign(CENTER, CENTER)
    if (placing.state === false) {
      textSize(cnv.w * 0.015)
      push()
      translate(mouseX, mouseY)
      text(txt[segmentCounter], 0, 0)
      pop()
    } else {
      textSize(cnv.w * placing.size)
      push()
      translate(windowWidth * 0.5 + placing.pos.x * cnv.w, windowHeight * 0.5 + placing.pos.y * cnv.h)
      rotate(placing.rotation)
      noStroke()
      fill(255)
      text(txt[segmentCounter], 0, 0)
      pop()
    }

    if (placing.state === false) {
      placing.pos.x = mouseX
      placing.pos.y = mouseY
    } else {
      noFill()
      stroke(255)
      strokeWeight(2)
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      line(placing.pos.x * cnv.w, placing.pos.y * cnv.h, placing.offset.x * cnv.w, placing.offset.y * cnv.h)
      pop()
    }
  }

  if (state.webcam === true) {
    textAlign(LEFT, TOP)
    textSize(cnv.w * 0.015)
    fill(255)
    noStroke()
    push()
    translate(windowWidth * 0.5 - cnv.w * 0.49, windowHeight * 0.5 - cnv.h * 0.49)
    if (segmentCounter < txt.length) {
      text('current segment:', 0, 0)
    } else {
      text('done!', 0, 0)
    }
    push()
    translate(cnv.w * 0.15, 0)
    text(txt[segmentCounter], 0, 0)
    pop()
    pop()

    push()
    translate(windowWidth * 0.5 + cnv.w * 0.375, windowHeight * 0.5 - cnv.h * 0.49)
    text('undo: backspace', 0, 0)
    pop()

    textAlign(CENTER, TOP)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5 - cnv.h * 0.49)
    text('click to place text', 0, 0)
    pop()

    if (segmentCounter < txt.length) {
      push()
      translate(windowWidth, 0)
      scale(-1, 1)
      tint(255, 0.25)
      image(capture, windowWidth * 0.5 - capture.width * 0.5, windowHeight * 0.5 - capture.height * 0.5)
      filter(GRAY)
      pop()

      if (pose) {
        var eyeR = pose.rightEye
        var eyeL = pose.leftEye
        var d = dist(eyeR.x, eyeR.y, eyeL.x, eyeL.y)
        push()
        translate(windowWidth, 0)
        noFill()
        stroke(255)
        strokeWeight(2)
        scale(-1, 1)
        push()
        translate(windowWidth * 0.5 - capture.width * 0.5, windowHeight * 0.5 - capture.height * 0.5)
        ellipse(pose.nose.x, pose.nose.y, d)
        ellipse(pose.rightWrist.x, pose.rightWrist.y, d * 0.25)
        ellipse(pose.leftWrist.x, pose.leftWrist.y, d * 0.25)
        for (var i = 0; i < skeleton.length; i++) {
          var a = skeleton[i][0]
          var b = skeleton[i][1]
          line(a.position.x, a.position.y, b.position.x, b.position.y)
        }
        pop()
        pop()
      }
    }
    
    if (pose) {
      blendMode(DIFFERENCE)
      textAlign(CENTER, CENTER)
      for (var i = 0; i < placed.length; i++) {
        textSize(cnv.w * placed[i][3])
        push()
        translate(placed[i][1] * cnv.w, placed[i][2] * cnv.h)
        rotate(placed[i][4])
        noStroke()
        fill(255)
        text(placed[i][0], 0, 0)
        pop()
      }

      placing.distance = dist(pose.rightWrist.x, pose.rightWrist.y, pose.leftWrist.x, pose.leftWrist.y) / dist(0, 0, capture.width, capture.height)
      placing.size = 0.015 + placing.distance * 0.25
      placing.rotation = -atan2(pose.leftWrist.y - pose.rightWrist.y, pose.leftWrist.x - pose.rightWrist.y)
      blendMode(DIFFERENCE)
      textAlign(CENTER, CENTER)
      textSize(cnv.w * placing.size)
      push()
      translate(cnv.w * (1 - pose.nose.x / capture.width), cnv.h * (pose.nose.y / capture.height))
      rotate(placing.rotation)
      noStroke()
      fill(255)
      text(txt[segmentCounter], 0, 0)
      pop()
    }
  }
}

function mousePressed() {
  if (state.mouse === true) {
    placing.state = true
    placing.pos.x = (mouseX - windowWidth * 0.5) / cnv.w
    placing.pos.y = (mouseY - windowHeight * 0.5) / cnv.h
  }
}

function mouseDragged() {
  if (state.mouse === true) {
    placing.offset.x = (mouseX - windowWidth * 0.5) / cnv.w
    placing.offset.y = (mouseY - windowHeight * 0.5) / cnv.h
    placing.distance = dist(placing.pos.x, placing.pos.y, placing.offset.x, placing.offset.y)
    placing.size = 0.015 + placing.distance * 0.25
    placing.rotation = -atan2(mouseX - (windowWidth * 0.5 + placing.pos.x * cnv.w), mouseY - (windowHeight * 0.5 + placing.pos.y * cnv.h)) + Math.PI * 0.5
  }
}

function mouseReleased() {
  if (state.menu === true) {
    if (mouseX <= windowWidth * 0.5) {
      state.menu = false
      setTimeout(function() {
        state.mouse = true
      }, 1)
    } else {
      state.menu = false
      setTimeout(function() {
        state.webcam = true
      }, 1)
    }
  }
  if (state.mouse === true) {
    placing.state = false
    placing.offset.x = (mouseX - windowWidth * 0.5) / cnv.w
    placing.offset.y = (mouseY - windowHeight * 0.5) / cnv.h
    placing.distance = dist(placing.pos.x, placing.pos.y, placing.offset.x, placing.offset.y)
    placing.size = 0.015 + placing.distance * 0.25
    placing.rotation = -atan2(mouseX - (windowWidth * 0.5 + placing.pos.x * cnv.w), mouseY - (windowHeight * 0.5 + placing.pos.y * cnv.h)) + Math.PI * 0.5
    placed.push([txt[segmentCounter], placing.pos.x, placing.pos.y, placing.size, placing.rotation])
    if (segmentCounter < txt.length) {
      segmentCounter++
    }
  }
  if (state.webcam === true) {
    placing.pos.x = 1 - pose.nose.x / capture.width
    placing.pos.y = pose.nose.y / capture.height
    placing.distance = dist(pose.rightWrist.x, pose.rightWrist.y, pose.leftWrist.x, pose.leftWrist.y) / dist(0, 0, capture.width, capture.height)
    placing.size = 0.015 + placing.distance * 0.25
    placing.rotation = -atan2(pose.leftWrist.y - pose.rightWrist.y, pose.leftWrist.x - pose.rightWrist.y)
    placed.push([txt[segmentCounter], placing.pos.x, placing.pos.y, placing.size, placing.rotation])
    if (segmentCounter < txt.length) {
      segmentCounter++
    }
  }
}

function keyPressed() {
  if (state.mouse === true || state.webcam === true) {
    if (segmentCounter > 0 && keyCode === 8) {
      placed.pop()
      segmentCounter--
    }
  }
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
  cnv.h = windowHeight * 0.95
  cnv.w = windowWidth * 0.95
}

function textProcessor(txt) {
  for (var i = txt.length - 1; i >= 0; i--) {
    if (txt[i][0] === '#') {
      txt.splice(i, 1)
    }
  }
  var list = []
  for (var i = 0; i < txt.length; i++) {
    if (txt[i][txt[i].length - 1] == '"') {
      txt[i] = txt[i].substring(1, txt[i].length - 1)
      splitted = txt[i].split(' ')
      list.push(splitted)
    } else {
      if (txt[i][txt[i].length - 1] == '0') {
        txt[i] = txt[i].substring(1, txt[i].length - 1)
        splitted = txt[i].split(' ')
        list.push(splitted)
      }
      if (txt[i][txt[i].length - 1] == '1') {
        txt[i] = txt[i].substring(1, txt[i].length - 4)
        list.push([txt[i]])
      }
      if (txt[i][txt[i].length - 1] == '2') {
        txt[i] = txt[i].substring(1, txt[i].length - 4)
        txt[i] = txt[i].replace(/\s/g, '')
        splitted = txt[i].split('')
        list.push(splitted)
      }
    }
  }
  list = list.flat()
  return list
}
